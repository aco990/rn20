/*
 * File Download via HTTP 1.1 GET Request
 * Copyright (C) 2020 by Thomas Dreibholz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contact: dreibh@simula.no
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <iostream>


// ###### Main program ######################################################
int main(int argc, char** argv)
{
   // ====== Check arguments ================================================
   if(argc < 4) {
      std::cerr << "Usage: " << argv[0] << " [Remote Host] [Remote Service or Port] [Requested File]" << std::endl;
      exit(1);
   }
   const char* remoteHost    = argv[1];
   const char* remoteService = argv[2];
   const char* requestedFile = argv[3];

   // ====== Get remote address (resolve hostname and service) ==============
   struct addrinfo* ainfo = NULL;
   struct addrinfo  ainfohint;
   memset((char*)&ainfohint, 0, sizeof(ainfohint));
   // ainfohint.ai_flags    = 0;
   ainfohint.ai_family   = PF_UNSPEC;
   ainfohint.ai_socktype = SOCK_STREAM;
   ainfohint.ai_protocol = IPPROTO_TCP;
   int error = getaddrinfo(remoteHost, remoteService, &ainfohint, &ainfo);
   if(error != 0) {
      std::cerr << "ERROR: getaddrinfo() failed: " << gai_strerror(error) << std::endl;
      exit(1);
   }

   // ====== Convert remote address to human-readable format ================
   char resolvedHost[NI_MAXHOST];
   char resolvedService[NI_MAXSERV];
   error = getnameinfo(ainfo->ai_addr, ainfo->ai_addrlen,
                       (char*)&resolvedHost, sizeof(resolvedHost),
                       (char*)&resolvedService, sizeof(resolvedService),
                       NI_NUMERICHOST);
   if(error != 0) {
      std::cerr << "ERROR: getnameinfo() failed: " << gai_strerror(error) << std::endl;
      exit(1);
   }
   std::cout << "Connecting to remote host "
             << resolvedHost << ", service " << resolvedService << " ..." << std::endl;

   // ====== Create socket of appropriate type ==============================
   int sd = socket(ainfo->ai_family, ainfo->ai_socktype, ainfo->ai_protocol);
   if(sd <= 0) {
      perror("Unable to create socket");
      exit(1);
   }

   // ====== Connect to remote node ==========================================
   if(connect(sd, ainfo->ai_addr, ainfo->ai_addrlen) < 0) {
      perror("connect() call failed");
      exit(1);
   }

   // ====== Request webpage =================================================
   std::cout << "Connected! Sending HTTP GET..." << std::endl;
   char httpGet[256];
   snprintf((char*)&httpGet, sizeof(httpGet),
            "GET %s HTTP/1.0\r\nHost: %s\r\nConnection: close\r\n\r\n",
            requestedFile, remoteHost);
   std::cout << "\x1b[37m" << httpGet << "\x1b[0m";
   if(write(sd, httpGet, strlen(httpGet)) < 0) {
      perror("write() call failed");
      exit(1);
   }

   // ====== Receive reply ===================================================
   std::cout << "Request sent. Waiting for answer..." << std::endl;
   for(;;) {
      char data[256];

      ssize_t received = read(sd, (char*)&data, sizeof(data));
      if(received < 0) {
         perror("read() call failed");
         break;
      }
      else if(received == 0) {
         // Connection closed without error.
         break;
      }
      else {
         std::cout << "\x1b[34m";
         for(size_t i = 0; i < (size_t)received; i++) {
            if(isprint(data[i])) {
               // This works for ASCII characters. It may be useful to add
               // UTF-8 character handling here, in order to support other
               // characters!
               std::cout << data[i];
            }
            else if(data[i] == '\n') {
               std::cout << std::endl;
            }
            else if(data[i] == '\r') {
            }
            else {
               std::cout << '.';
            }
         }
         std::cout << "\x1b[0m";
      }
   }
   std::cout << std::endl;

   // ====== Clean up =======================================================
   freeaddrinfo(ainfo);
   close(sd);
   return 0;
}
